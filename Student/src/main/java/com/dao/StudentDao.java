package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {
	
	@Autowired
	StudentRepository studentRepository;

	public Student addStudent(Student student) {
		return studentRepository.save(student);
	}

	public List<Student> getAllStudents() {
		
		return studentRepository.findAll();
	}

	public Student getStudentById(int id) {
		
		Student student = new Student(0,"not found",0,"notfound");

		return studentRepository.findById(id).orElse(student);
	}

	public List<Student> getStudentByName(String name) {
		return studentRepository.findByName(name);
	}

	public Student updateStudent(Student student) {
		
		return studentRepository.save(student);
	}

	public void deleteStudentById(int id) {
		
		studentRepository.deleteById(id);
	}


}

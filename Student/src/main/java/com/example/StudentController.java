package com.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	
	@Autowired
	StudentDao studentdao;
	
	@PostMapping("/addStudent")
	public Student addStudent(@RequestBody Student student){
		return studentdao.addStudent(student);
	}
	
	@GetMapping("/getAllStudents")
	public List<Student> getAllStudents(){
		
		return studentdao.getAllStudents();
		
	}
	 
	@GetMapping("/getStudentById/{id}")
	public Student getStudentById(@PathVariable int id){
		return studentdao.getStudentById(id);
	}
	
	@GetMapping("/getStudentByName/{name}")
	public List<Student> getStudentByName(@PathVariable("name") String name){
		return studentdao.getStudentByName(name);
	}
	
	@PostMapping("/updateStudent")
	public Student updateStudent(@RequestBody Student student){
		return studentdao.updateStudent(student);
	}
	
	@DeleteMapping("/deletestudentbyid/{id}")
	public String deleteStudentById(@PathVariable int id){
		studentdao.deleteStudentById(id);
		return "student deleted";
	}

}

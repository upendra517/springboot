package com.model;

import java.util.Date;

public class Employee {
	
	int employeeId;
	String employeeName;
	long salary;
	String gender;
	Date dateOfJoining;
	String country;
	String emailId;
	String password;
	
	public Employee() {
		
	}

	public Employee(int employeeId, String employeeName, long salary, String gender, Date dateOfJoining, String country,
			String emailId, String password) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.salary = salary;
		this.gender = gender;
		this.dateOfJoining = dateOfJoining;
		this.country = country;
		this.emailId = emailId;
		this.password = password;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", salary=" + salary
				+ ", gender=" + gender + ", dateOfJoining=" + dateOfJoining + ", country=" + country + ", emailId="
				+ emailId + ", password=" + password + "]";
	}
}
